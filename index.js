//To install npm packages in the repository simultaneously: enter in the terminal 'npm install express nodemon mongoose' or 'npm i express nodemon mongoose'

const express = require('express');
const mongoose = require('mongoose');

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Syntax: mongoose.connect('<connection string>, {middleware}')
//change <password> to your password on your admin
//change MyFirstDatabase to your own Data Collection that you like.
mongoose.connect('mongodb+srv://admin:admin1234@zuittbootcamp.p5gi4.mongodb.net/session30?retryWrites=true&w=majority', 
	{
		//to avoid future and current errors
		useNewUrlParser: true,
		useUnifiedTopology:true
	}
);

let db = mongoose.connection;
	
	//console.error.bind(console) - print errors in the browser and in the terminal
	db.on("error",console.error.bind(console,"Connection Error"));

	// if the connection is successful, this will be the output in our console.
	db.once('open',() => console.log('Connected to the cloud database'));

// Mongoose Schema - this will be the blueprint of our database

const taskSchema = new mongoose.Schema({

	// define the name of our schema - taskSchema
	// new mongoose.Schema method - to make a schema
	// we will be needing the name of the task and its status
	// each field will require a data type
	
	name: String,
	status:{
		type: String,
		default: 'pending'
	}
});

//models use schemas and they act as the middleman from the server to our database
// Model can now be used to run commands for interacting with our database.
// name of model should be capitalized and singular form - 'Task'.
// second parameter is used to specify the schema of the documents that will be stored in the mongoDB collection.
const Task = mongoose.model('Task', taskSchema);


/*
Business Logic
	 
	1. Add a functionality to check if there are duplicate tasks 
		-if the task already exists in the db, we return an error
		-if the task doesn't exists in the db, we add it in the db.
	2. The task data will be coming from the request body.
	3. Create a new Task object with name field property.
 */

app.post('/tasks',(req,res) => {


	// check any duplicate task
	// err shorthand of erron
	Task.findOne({name: req.body.name}, (err,result) => {

		// if there are matches, 
		if(result != null && result.name === req.body.name){

			//will return this message
			return res.send('Duplicate task found.');

		} else {

			// Create a new task
			let newTask = new Task({

				name: req.body.name
			})

			// save method will accept a callback function which stores any errors in the first parameter and will store the newly saved document in the second parameter.
			newTask.save((saveErr,savedTask) =>{

				//if there are any error, it will print the error.
				if(saveErr){
					return console.error(saveErr)

				//no error found, return the status code and message.
				}else{
					return res.status(201).send('New Task Created')
				}
			})
		}
	})
})


// Retrieving All Tasks

app.get('/tasks',(req,res) =>{
	Task.find({}, (err,result) =>{

		if(err){

			return console.log(err);

		}else{

			return res.status(200).json({
				data: result
			})
		}
	})
})


// Activity

//User Schema
const userSchema = new mongoose.Schema({

	name: String,
	password:String,
});

// User Model
const User = mongoose.model('User', userSchema);

// Business Logic
app.post('/signup',(req,res) => {


	User.findOne({name: req.body.name}, (err,result) => {


		if(result != null && result.name === req.body.name && result.password === req.body.password){


			return res.send('Duplicate User found. Please Enter Another User Again.');

		} else {

			let newUser = new User({

				name: req.body.name,
				password: req.body.password
			})


			newUser.save((saveErr,savedTask) =>{

	
				if(saveErr){
					return console.error(saveErr)


				}else{
					return res.status(201).send('New User Has Been Created')
				}
			})
		}
	})
})


// Retrieving All Users

app.get('/users',(req,res) =>{
	User.find({}, (err,result) =>{

		if(err){

			return console.log(err);

		}else{

			return res.status(200).json({
				data: result
			})
		}
	})
})


app.listen(port, () => console.log(`Server is running at port ${port}.`));

